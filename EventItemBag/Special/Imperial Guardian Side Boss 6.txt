3
//Index   DropRate
0         10000
1	      10000
end

4
//Index   Section   SectionRate   MoneyAmount   OptionValue   DW   DK   FE   MG   DL   SU   RF   GL   RW   SL   GC
0         5         10000         100           32            1    1    1    1    1    1    1    1    1    1    1  
1         6         10000         100           32            1    1    1    1    1    1    1	 1    1    1    1 
end

5
//Index   Level   Grade   Option0   Option1   Option2   Option3   Option4   Option5   Option6   Duration Comment 
14,485    0       0       *         *         *         *         *         *         *         0         //Ruud Box (500)
end

6  //Seed sphere a 1 3
//Index   Level   Grade   Option0   Option1   Option2   Option3   Option4   Option5   Option6   Duration Comment
6244      0       0       600       *         *         *         *        *         0         0         //Seed Sphere (Fire)"      
6245      0       0       610       *         *         *         *        *         0         0         //Seed Sphere (Water)"     
6246      0       0       610       *         *         *         *        *         0         0         //Seed Sphere (Ice)"       
6247      0       0       600       *         *         *         *        *         0         0         //Seed Sphere (Wind)"      
6248      0       0       620       *         *         *         *        *         0         0         //Seed Sphere (Lightning)" 
6249      0       0       620       *         *         *         *        *         0         0         //Seed Sphere (Earth)"     
6250      0       0       600       *         *         *         *        *         0         0         //Seed Sphere (Fire)"      
6251      0       0       610       *         *         *         *        *         0         0         //Seed Sphere (Water)"     
6252      0       0       610       *         *         *         *        *         0         0         //Seed Sphere (Ice)"       
6253      0       0       600       *         *         *         *        *         0         0         //Seed Sphere (Wind)"      
6254      0       0       620       *         *         *         *        *         0         0         //Seed Sphere (Lightning)" 
6255      0       0       620       *         *         *         *        *         0         0         //Seed Sphere (Earth)"     
6256      0       0       600       *         *         *         *        *         0         0         //Seed Sphere (Fire)"      
6257      0       0       610       *         *         *         *        *         0         0         //Seed Sphere (Water)"     
6258      0       0       610       *         *         *         *        *         0         0         //Seed Sphere (Ice)"       
6259      0       0       600       *         *         *         *        *         0         0         //Seed Sphere (Wind)"      
6260      0       0       620       *         *         *         *        *         0         0         //Seed Sphere (Lightning)" 
6261      0       0       620       *         *         *         *        *         0         0         //Seed Sphere (Earth)" 
7431      0       0       -1        -1        -1        -1        -1       -1        -1        0         //Bless of Light (Low Grade) 
7431      0       0       -1        -1        -1        -1        -1       -1        -1        0         //Bless of Light (Low Grade) 
end