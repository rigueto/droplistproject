3
//Index   DropRate
0         10000
1         10000
2         10000
3         10000 
end

4
//Index   Section   SectionRate   MoneyAmount   OptionValue   DW   DK   FE   MG   DL   SU   RF   GL   RW   SL   GC
0         5         2000          0             04            1    1    1    1    1    1	1    1    1    1    1
0         6         8000          100           32            1    1    1    1    1    1    1    1    1    1    1
1         7         10000         100           04            1    1    1    1    1    1	1    1    1    1    1
2         8         5000          100           04            1    1    1    1    1    1	1    1    1    1    1
2         9         5000          100           04            1    1    1    1    1    1	1    1    1    1    1
3         10        10000         100           32            1    1    1    1    1    1	1    1    1    1    1
end

5
//Index   Level   Grade   Option0   Option1   Option2   Option3   Option4   Option5   Option6   Duration Comment 
006354      0       0       *         *         *         *         *         *         *         0         //Empire Guardians' Stronghold" 
1,05    0       0       *         *         *         *         *         *         *         0         //Ruud Box (1500)
end

6
//Index   Level   Grade   Option0   Option1   Option2   Option3   Option4   Option5   Option6   Duration   Comment
14,506      0       0       *         *         *         *         *        *        5         0      //Ruud Box (5000)         
6357        0       0       *         *         *         *         *        *        5         0      //Antonia's Sword"                 
6450        0       0       *         *         *         *         *        *        5         0      //Runedil's Goldentune Harp" 
6451        0       0       *         *         *         *         *        *        5         0      //Lemuria's Orb"             
6452        0       0       *         *         *         *         *        *        5         0      //Norrwen's Bloodstring Lyra"  
end
 
7
//Index   Level   Grade   Option0   Option1   Option2   Option3   Option4   Option5   Option6   Duration Comment
14,58      0       0       *         *         *         *         *        *         0         0         //Rare Ticket Lvl1
14,58      0       0       *         *         *         *         *        *         0         0         //Rare Ticket Lvl1
14,58      0       0       *         *         *         *         *        *         0         0         //Rare Ticket Lvl1
14,58      0       0       *         *         *         *         *        *         0         0         //Rare Ticket Lvl1
14,58      0       0       *         *         *         *         *        *         0         0         //Rare Ticket Lvl1
end

8
//Index   Level   Grade   Option0   Option1   Option2   Option3   Option4   Option5   Option6   Duration Comment   
7510      0       0       *         *         *         *         *        *         0         0         //"Steel of Heaven"
7510      0       0       *         *         *         *         *        *         0         0         //"Steel of Heaven"
end

9
//Index   Level   Grade   Option0   Option1   Option2   Option3   Option4   Option5   Option6   Duration Comment
7635      0       0       *         *         *         *         *        *         0         0         //Guardian Enhanced Stone
7635      0       0       *         *         *         *         *        *         0         0         //Guardian Enhanced Stone
end

10
//Index   Level   Grade   Option0   Option1   Option2   Option3   Option4   Option5   Option6   Duration Comment   
7479      0       0       *         *         *         *         *        *         0         0         //Fragment of Radiance slot (3)
end