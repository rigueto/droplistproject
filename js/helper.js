 
$(function(){

    $(".droplist").click(function(e){
		//Disable all buttons.
		$(':button').prop('disabled', true);

		let classes = this.getAttribute('class');
		console.log(classes);

		let serializedData = {
			userSearch : classes
		},
		
		request = $.ajax({
			url: "/finder.php",
			type: "POST",
			async:true,
			data: serializedData,
		});

		// Callback handler that will be called on success
		request.done(function (response, textStatus, jqXHR){
			//If is success, call createDom with response.
			//console.log(response);
			createDom(response);
		});

		// Callback handler that will be called on failure
		request.fail(function (jqXHR, textStatus, errorThrown){
			// Log the error to the console
			console.error(
				"The following error occurred: "+
				textStatus, errorThrown
			); 
		});

		// Callback handler that will be called regardless
		// if the request failed or succeeded
		request.always(function () {
			// Reenable the inputs
			$(':button').prop('disabled', false);
		}).responseJSON; 

    })
 
});


function createDom(response) { 
	// Log a message to the console.
	splitResp = response.split('Array');

	for(var i=2; i < splitResp.length; i++){

		splitLine = splitResp[i].split('=>'); 

		for(var y=1; y < splitLine.length; y++){
			if(splitLine[y].includes('\n')) {
				let splitLineInside = splitLine[y].split('\n');
				let infoDrop = splitLineInside[0].trim().split('\n')[0];

				//IndexPai
				if(y === 1) {
					console.log('IndexPai: '+infoDrop);
				//IndexFilho	
				}else if(y === 2) {
					console.log('IndexFilho: '+infoDrop);
				//Level
				}else if(y === 3) {
					console.log('Level: '+infoDrop);
				//Grade
				}else if(y === 4) {
					console.log('Grade: '+infoDrop);
				//Option0
				}else if(y === 5) {
					console.log('Option0: '+infoDrop);
				//Option1
				}else if(y === 6) {
					console.log('Option1: '+infoDrop);
				//Option2
				}else if(y === 7) {
					console.log('Option2: '+infoDrop);
				//Option3
				}else if(y === 8) {
					console.log('Option3: '+infoDrop);
				//Option4
				}else if(y === 9) {
					console.log('Option4: '+infoDrop);
				//Option5
				}else if(y === 10) {
					console.log('Option5: '+infoDrop);
				//Option6
				}else if(y === 11) {
					console.log('Option6: '+infoDrop);
				//Duration
				}else if(y === 12) {
					console.log('Duration: '+infoDrop);
				//name
				}else if(y === 13) {
					console.log('name: '+infoDrop);
				}           
			}		
		}
	}
}
