<!--link href="css/custom.css" rel="stylesheet"-->
<script src="https://code.jquery.com/jquery-3.6.0.js" integrity="sha256-H+K7U5CnXl1h5ywQfKtSj8PCmoN9aaq30gDh27Xc0jk=" crossorigin="anonymous"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
<script src="js/helper.js"></script> 

<?php

require_once 'class/class.eventitembag.php'; 
 
$searchBagPost = filter_input(INPUT_POST, 'userSearch', FILTER_SANITIZE_STRING);
//$searchBagPost = $_POST['userSearch'];

$dirBase = getcwd();

if(isset($searchBagPost)) {
    if (strpos($searchBagPost, 'droplist') !== false)  {
        
        $searchBagPost = explode(' ', $searchBagPost);

        $lenghtBag = sizeof($searchBagPost);

        if($lenghtBag == 4) {
            $bagOrItemDrop = $searchBagPost[1];
            $category =  $searchBagPost[2];
            $bagName =  $searchBagPost[3].'.txt';
        }else{
            $bagOrItemDrop = $searchBagPost[1];
            $category =  $searchBagPost[2];
            $bagNameSplit = '';
            
            for($i = 3; $i < $lenghtBag; $i++) {
                $bagNameSplit .= $searchBagPost[$i] . ' ';     
            }
            $bagName =  trim($bagNameSplit).'.txt';
        }
        $dirComplete = $dirBase . '/' . $bagOrItemDrop . '/' . $category;

        $search = new SearchEventItemBag();

        //Método para verificar os drops habilitados na eventbag.
        $dropEnabled = $search->eventBagFirstSection($bagName, $dirComplete);
    
        //Método para verificar os rates dos drops habilitados e se vão para 
        //Inventório de eventos ou para o chão.
        $secondSection = $search->eventBagSecondSection($bagName, $dropEnabled, $dirComplete);
     
        print_r($search->eventBagDropSection($bagName, $secondSection, $dirComplete, $dirBase));
    }
}
 






