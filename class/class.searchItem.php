<?php

class SearchItem {

    public function item($indexAll, $dirBase) {

        //ini_set("memory_limit","2048M");

        //Armazena a versão original sem add novos arrays.
        $indexSecond = $indexAll;
        $dir = $dirBase . '/item/Item.txt';

        //$dir = getcwd();
        //Lê um arquivo em um array. 
        $lines = file($dir);
        //Array criado pra guardar os nomes dos itens no foreach abaixo.
        $itensName = array();
        //Array criado pra guardar os nomes dos itens no foreach abaixo.
        $indexItemAll = array();
        //Array criado pra guardar os index pai.
        $indexPai = array();
        //Array criado pra guardar tudo.
        $indicesKeys = [];

        for($y=0; $y <= sizeof($indexSecond); $y++) {
            //$bagSize = 1;
            //if(sizeof($indexSecond) > 51) {
            //    $bagSize = sizeof($indexSecond) / 50;
            //} 
            //for($y=1; $y <= ceil($bagSize); $y++) {

            foreach ($lines as $line_number => $line) {
                //echo "Linha #<b>{$line_num}</b> : " . $teste[0] . "<br>\n";
                //Pegando apenas o index do item.

                //Replace mais de 1 espaço pra 1 e limpa caracteres especiais.
                $indexItem = explode(' ', htmlspecialchars(preg_replace('/\s\s+/', ' ', $line)));
                //$indexItem = array_splice($indexItem, 0, 14);
                //$indexItem = trim($indexItem);
                //Se não existir algo no 3º array, significa que depois do explode estamos no index paaai.  
                if(!isset($indexItem[3])) {
                    if(trim($indexItem[0]) != 'end' && substr($indexItem[0], 0, 2) != "//") {
                        array_push($indexPai, trim($indexItem[0]));
                        $indexPaiNoArray = trim($indexItem[0]);
                    }
                }
            
                //Add todos index no array.
                array_push($indexItemAll, $indexItem[0]);

                //Pegando os nomes dos itens.
                preg_match_all('/".*?"/', $line, $matchAll);

                //Add os itens no array criado para isso.
                array_push($itensName, $matchAll);

                if(isset($matchAll[0][0])) {
                    //Sanitizando.
                    try{
                        $matchAll[0][0] = preg_replace("/[^a-zA-Z0-9 ]+/", "", html_entity_decode($matchAll[0][0], ENT_QUOTES));
                    }catch(Exception $e) {
                        return $e->getMessage();
                    }

                    $indexPaiNoArray = trim($indexPaiNoArray);
                    $indexItem[0] = trim($indexItem[0]);

                    if(strlen($indexPaiNoArray) >= 2) {
                        $indexPaiNoArray = ltrim($indexPaiNoArray, '0');
                    }
                    if(strlen($indexItem[0]) >= 2) {
                        $indexItem[0] = ltrim($indexItem[0], '0');
                    }

                    //Add os itens no array criado para isso.
                    $indicesKeys['indexPai'] = $indexPaiNoArray;
                    $indicesKeys['indexFilho'] =  $indexItem[0];
                    
                    $indicesKeys['indexConvertido'] = strval(intval(trim($indexPaiNoArray)) * 512 + intval($indexItem[0]));
                    
                    $indicesKeys['ItemName'] = trim($matchAll[0][0]);

                    array_push($indexAll,  $indicesKeys);
                }
            }
        }

        foreach ($indexSecond as $keySec => $indexSecondUnique) {
 
            foreach ($indexAll as $key => $indexUnique) { 

                if(isset($indexUnique['indexPai']) && isset($indexUnique['indexFilho']) && isset($indexUnique['indexConvertido'])) { 

                    $indexPaieFilho = $indexUnique['indexPai'].','.$indexUnique['indexFilho'];

                    if(($indexSecondUnique['IndexFilho'] === $indexUnique['indexConvertido']) || ($indexSecondUnique['IndexFilho'] === $indexPaieFilho)) {
                        //Add os nomes dos itens.
                        $indexSecondUnique['name'] = $indexUnique['ItemName']; 

                        break;
                    }
                }
            }
            //Achou para o for e grava no array, volta pra próxima posição pra procurar.
            array_push($indexSecond, $indexSecondUnique);
        }
        
        //workaround horroroso pra está duplicando o foreach acima sem o name.
        for($arrayIni = 0; $arrayIni < sizeof($indexSecond); $arrayIni++) {
            if(isset($indexSecond[$arrayIni]['name'])) {
                break;
            }
        }

        //Removendo arrays iniciais sem name.
        return array_splice($indexSecond, $arrayIni);
    } 
}
