<?php

require_once 'class/class.searchItem.php'; 

class SearchEventItemBag {

    public function eventBagFirstSection($eventBag, $dir) {
        //Pegando current directory e add /EventItemBag
        //$dir = getcwd().'/EventItemBag';
        //Verificando diretório.
        //$dh = scandir($dir);
        //$fileNames = array_splice($dh, 2);         
        //Lê um arquivo em um array. 
        //$lines = file($dir.'/'.$eventBag.'.txt');

        $lines = file($dir.'/'.$eventBag);

        //Array criado pra guardar os nomes dos itens no foreach abaixo.
        $indexItemAll = array();
        //Array criado pra guardar os index pai.
        $indexPai = array();
        //Array criado pra guardar tudo.
        $indicesKeys = [];
        //Array criado pra guardar tudo.
        $indexAll = array();

        $lineOfDropRateOne = '';
        $lineOfDropRateEndOne = '';
        //Pega o array do match do preg e faz um for passando por todos e
        //Add no array os nome entre aspas duplas.
        foreach ($lines as $line_number => $line) {
        //echo "Linha #<b>{$line_num}</b> : " . $teste[0] . "<br>\n";

            $indexItem = explode(' ', htmlspecialchars(preg_replace('/\s\s+/', ' ', $line))); 

            //Procurando o index Pai.
            if(trim($indexItem[0]) != 'end' && trim($indexItem[0]) == 3)  {
                array_push($indexPai, trim($indexItem[0]));
                $indexPaiNoArray = trim($indexItem[0]);
                $lineOfDropRateOne = $line_number;
            }

            //Pegando primeiro end, da seção 3.
            if(trim($indexItem[0]) == 'end') {
                // && $lineOfDropRateEndOne == '' && $lineOfDropRateOne != '' && intval($line_number) > intval($lineOfDropRateOne)
                $lineOfDropRateEndOne = $line_number;
            }

            //lineOfDropRateEnd deve ser vazio, pois ao chegar na linha específica ele recebe a posição do end e para de entrar.
            if($lineOfDropRateOne != '' && $lineOfDropRateEndOne == '' && substr($indexItem[0], 0, 2) != "//") {

                //Verifica se não chegou ao fim do index 3.
                if($line_number > $lineOfDropRateOne) {
                    //Add todos index no array.
                    if(is_numeric($indexItem[0])) {
                        $indexPaiNoArray = trim($indexPaiNoArray);
                        $indexItem[0] = trim($indexItem[0]);

                        if(strlen($indexPaiNoArray) >= 2) {
                            $indexPaiNoArray = ltrim($indexPaiNoArray, '0');
                        }
                        if(strlen($indexItem[0]) >= 2) {
                            $indexItem[0] = ltrim($indexItem[0], '0');
                        }

                        array_push($indexItemAll, $indexItem[0]);
                        $indicesKeys['indexDropOne'] = trim($indexPaiNoArray);
                        $indicesKeys['dropCode'] =  $indexItem[0];   
                    }
       
                    array_push($indexAll,  $indicesKeys);
                }
                
            }else{
                //End alimenta essa variável, quando ela tiver diferente de vazio
                //Encerra o processo de capturar todos index do drop seção 3.
                if($lineOfDropRateEndOne != '') {
                    return $indexAll;
                }
            }
        }
    }

    public function eventBagSecondSection($eventBag, $dropEnabled, $dir) {
        //Pegando current directory e add \EventItemBag
        //$dir = getcwd().'\EventItemBag';
        //Verificando diretório.
        //$dh = scandir($dir);
        //$fileNames = array_splice($dh, 2);         
        //Lê um arquivo em um array. 
        //$lines = file($dir.'\\'.$eventBag.'.txt');
        $lines = file($dir.'/'.$eventBag);
        //Array criado pra guardar os nomes dos itens no foreach abaixo.
        //$itensName = array();
        //Array criado pra guardar os nomes dos itens no foreach abaixo.
        $indexItemAll = array();
        //Array criado pra guardar os index pai.
        $indexPai = array();
        //Array criado pra guardar tudo.
        $indicesKeys = [];
        //Array criado pra guardar tudo.
        $indexAll = array();
        $indexPaiNoArray = '';
        $lineOfDropRate = '';
        $lineOfDropRateEnd = '';
        //Pega o array do match do preg e faz um for passando por todos e
        //Add no array os nome entre aspas duplas.
        foreach ($lines as $line_number => $line) {
            //echo "Linha #<b>{$line_number}</b> : " . $line . "<br>\n";
            $indexItem = explode(' ', htmlspecialchars(preg_replace('/\s\s+/', ' ', $line))); 

            //Procurando o index Pai.
            if(trim($indexItem[0]) != 'end' && trim($indexItem[0]) == 4)  {
                array_push($indexPai, $indexItem[0]);
                $indexPaiNoArray = $indexItem[0];
                $lineOfDropRate = $line_number;
            }

            if(trim($indexItem[0]) == 'end' && $lineOfDropRateEnd == '' && $lineOfDropRate != '' && $line_number > $lineOfDropRate) {
                $lineOfDropRateEnd = $line_number;
            }

            //Verifica se é numerico para evitar add 
            //Item index e etc. is numeric esta ignorando o primeiro 0 do index
            //Por isso o || comparando com 0 tb.
            //Verifica se a linha do index do droprate já foi definida para olhar somente dentro e
            //lineOfDropRateEnd deve ser vazio, pois ao chegar na linha específica ele recebe a posição do end e para de entrar.
            if($lineOfDropRate != '' && $lineOfDropRateEnd == '' && substr($indexItem[0], 0, 2) != "//") {

                //Verifica se não chegou ao fim do index 4.
                if($line_number > $lineOfDropRate) {
                    //Add todos index no array.
                    if(is_numeric($indexItem[0])) {
                        $indexPaiNoArray = trim($indexPaiNoArray);
                        $indexItem[0] = trim($indexItem[0]);

                        if(strlen($indexPaiNoArray) >= 2) {
                            $indexPaiNoArray = ltrim($indexPaiNoArray, '0');
                        }
                        if(strlen($indexItem[0]) >= 2) {
                            $indexItem[0] = ltrim($indexItem[0], '0');
                        }

                        array_push($indexItemAll, $indexItem[0]);

                        $indicesKeys['IndexPai'] = $indexPaiNoArray;
                        $indicesKeys['IndexFilho'] = $indexItem[0];  
                    }
                    if(isset($indexItem[1])) {
                        $indicesKeys['Section'] = trim($indexItem[1]);
                    }
                    if(isset($indexItem[2])) {
                        $indicesKeys['SectionRate'] = trim($indexItem[2]);
                    }
                    if(isset($indexItem[3])) {
                        $indicesKeys['MoneyAmount'] = trim($indexItem[3]);
                    }
                    if(isset($indexItem[3])) {
                        $indicesKeys['OptionValue'] = trim($indexItem[4]);
                    }              
                    array_push($indexAll,  $indicesKeys);
                }
            }
        }
        
        //Para cada drop da seção é passado em todo array do dropenabled.
        foreach($indexAll as $keyCod => $indexEach) {

            //Verificando se encontra o drop na seção do dropenabled.
            foreach($dropEnabled as $keyDrop => $dropEach) { 
 
                //Se encontrar ok, para e segue o fluxo.
                if($indexEach['IndexFilho'] == $dropEach['dropCode']) {
                    break;
                //Caso não encontre, inicia o processo de conferir se varreu
                //todas posições do array para depois sim remover ou não.
                }else{
                    //Se não encontrar o drop habilitado da primeira seção.
                    //Compara a quantidade com as vezes passadas pelo for
                    //Garantindo que foi conferido em todas posições do dropenabled.
                    //Caso não encontre, remove do array o drop, pois não está habilitado.
                    if($keyDrop+1 == sizeof($dropEnabled)) {
                        unset($indexAll[$keyCod]);
                    }
                } 
            }
        }
        return $indexAll; 
    }

    public function eventBagDropSection($eventBag, $secondSection, $dir, $dirBase) {
        //Pegando current directory e add \EventItemBag
        //$dir = getcwd().'\EventItemBag';
        //Verificando diretório.
        //$dh = scandir($dir);
        //$fileNames = array_splice($dh, 2);         
        //Lê um arquivo em um array. 
        //$lines = file($dir.'\\'.$eventBag.'.txt');
        $lines = file($dir.'/'.$eventBag);
        //Array criado pra guardar os index pai.
        $indexPai = array();
        //Array criado pra guardar tudo.
        $indexAll = array();
        //Array criado pra guardar os nomes dos itens no foreach abaixo.
        $indexItemAll = array();
        //Array criado pra guardar tudo.
        $indicesKeys = [];

        foreach ($secondSection as $idSecond) {
 
            $indexPaiNoArray = '';
            $lineOfDropRate = '';
            $lineOfDropRateEnd = ''; 
            //Pega o array do match do preg e faz um for passando por todos e
            //Add no array os nome entre aspas duplas.
            foreach ($lines as $line_number => $line) {
                //echo "Linha #<b>{$line_number}</b> : " . $line . "<br>\n";
                $indexItem = explode(' ', htmlspecialchars(preg_replace('/\s\s+/', ' ', $line))); 

                //Procurando o index Pai.
                if(trim($indexItem[0]) != 'end' && trim($indexItem[0]) == $idSecond["Section"])  {

                    $indexItem[0] = trim($indexItem[0]);

                    if(strlen($indexItem[0]) >= 2) {
                        $indexItem[0] = ltrim($indexItem[0], 0);
                    }

                    array_push($indexPai, $indexItem[0]);
                    $indexPaiNoArray = $indexItem[0];
                    $lineOfDropRate = $line_number;
                }

                if(trim($indexItem[0]) == 'end' && $lineOfDropRateEnd == '' && $lineOfDropRate != '' && $line_number > $lineOfDropRate) {
                    $lineOfDropRateEnd = $line_number;
                }

                //Verifica se é numerico para evitar add 
                //Item index e etc. is numeric esta ignorando o primeiro 0 do index
                //Por isso o || comparando com 0 tb.
                //Verifica se a linha do index do droprate já foi definida para olhar somente dentro e
                //lineOfDropRateEnd deve ser vazio, pois ao chegar na linha específica ele recebe a posição do end e para de entrar.
                if($lineOfDropRate != '' && $lineOfDropRateEnd == '' && substr($indexItem[0], 0, 2) != "//") {

                    //Verifica se não chegou ao fim do index x.
                    if($line_number > $lineOfDropRate) {
                        //Verifica se começa sem o // de comentado.
                        if(substr($indexItem[0], 0, 2) != "//") {

                            $indexPaiNoArray = trim($indexPaiNoArray);
                            $indexItem[0] = trim($indexItem[0]);
     
                            if(strlen($indexPaiNoArray) >= 2) {
                                if($indexPaiNoArray == 00) {
                                    $indexPaiNoArray = 0;
                                }else{
                                    $indexPaiNoArray = ltrim($indexPaiNoArray, 0);
                                }
                            }

                            //Verificando se há mais de 2 caracteres na string.
                            if(strlen($indexItem[0]) >= 2) {

                                //Se encontrar virgula trata os 2 valores.
                                if(strpos($indexItem[0], ',')) { 
                                    //Splita a string pra tratar os 2 valores.
                                    $indexItemSplit = explode(',', $indexItem[0]);

                                    if(strlen($indexItemSplit[0]) >= 2) {
                                        if($indexItemSplit[0] == 00) {
                                            $indexItemSplit[0] = 0;
                                        }else{
                                            $indexItemSplit[0] = ltrim($indexItemSplit[0], 0);
                                        }
                                    }
                                    if(strlen($indexItemSplit[1]) >= 2) {
                                        $indexItemSplit[1] = ltrim($indexItemSplit[1], 0);
                                    }

                                    //Junta primeira e segunda parte tratada com virtula no meio.
                                    $indexItem[0] = $indexItemSplit[0]. ','.$indexItemSplit[1];
                                }else{
                                    //Quando tiver 0 a esquerda do nro convertido.
                                    $indexItem[0] = ltrim($indexItem[0], 0);
                                }

                                //Reatribui o valor, caso passe no strlen acima.
                                $indexItem[0] = $indexItem[0];
                            }

                            array_push($indexItemAll, $indexItem[0]);

                            //array_push($indexItemAll, $indexItem[0]);
                            $indicesKeys['IndexPai'] = $indexPaiNoArray;
                            $indicesKeys['IndexFilho'] = $indexItem[0];  
                        
                            if(isset($indexItem[1])) {
                                $indicesKeys['Level'] = trim($indexItem[1]);
                            }
                            if(isset($indexItem[2])) {
                                $indicesKeys['Grade'] = trim($indexItem[2]);
                            }
                            if(isset($indexItem[3])) {
                                $indicesKeys['Option0'] = trim($indexItem[3]);
                            }
                            if(isset($indexItem[4])) {
                                $indicesKeys['Option1'] = trim($indexItem[4]);
                            }         
                            if(isset($indexItem[5])) {
                                $indicesKeys['Option2'] = trim($indexItem[5]);
                            }                    
                            if(isset($indexItem[6])) {
                                $indicesKeys['Option3'] = trim($indexItem[6]);
                            } 
                            if(isset($indexItem[7])) {
                                $indicesKeys['Option4'] = trim($indexItem[7]);
                            } 
                            if(isset($indexItem[8])) {
                                $indicesKeys['Option5'] = trim($indexItem[8]);
                            } 
                            if(isset($indexItem[9])) {
                                $indicesKeys['Option6'] = trim($indexItem[9]);
                            } 
                            if(isset($indexItem[10])) {
                                $indicesKeys['Duration'] = trim($indexItem[10]);
                            } 
                        }
                        array_push($indexAll,  $indicesKeys);
                    }
                }
            }
        }
        //Confere o item.txt
        if(!empty($indexAll)) {
            $itemsName = new SearchItem();
            return $itemsName->item($indexAll, $dirBase);
        }else{
            //Tratar casos de apenas drop ruud ou zen.
            return 'nada';
        }
    }
}
